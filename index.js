/**
 * @format
 */

import {AppRegistry} from 'react-native';
import ExportApp from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ExportApp);
