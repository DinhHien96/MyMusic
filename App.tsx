import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MyStack from './src/navigation';

const App = () => {
  return <NavigationContainer>
    <MyStack/>
  </NavigationContainer>;
};

const ExportApp = App;

export default ExportApp;
