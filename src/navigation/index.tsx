import React, {memo} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Routes from 'configs/Routes';
import Home from 'screen/Home';
import Options from 'screen/Options';
import BottomTab from 'navigation/bottomTab';
import LikeSongs from 'screen/LikeSongs';
import Settings from 'screen/Settings';
import NowPlaying from 'screen/NowPlaying';
import Discover from 'screen/Discover';
import VideoPlayer from 'screen/NowPlaying/VideoPlayer';
import SeekBar from 'screen/NowPlaying/SeekBar';
const Stack = createStackNavigator();
const MyStack = memo(() => {
  return (
    <Stack.Navigator
      initialRouteName={Routes.BottomTab}
      screenOptions={{headerShown: false}}>
      <Stack.Screen name={Routes.Home} component={Home} />
      <Stack.Screen name={Routes.Options} component={Options} />
      <Stack.Screen name={Routes.BottomTab} component={BottomTab} />
      <Stack.Screen name={Routes.LikeSongs} component={LikeSongs} />
      <Stack.Screen name={Routes.Settings} component={Settings} />
      <Stack.Screen name={Routes.NowPlaying} component={NowPlaying} />
      <Stack.Screen name={Routes.Discover} component={Discover} />
      <Stack.Screen name={Routes.VideoPlayer} component={VideoPlayer} />
      <Stack.Screen name={Routes.SeekBar} component={SeekBar} />
    </Stack.Navigator>
  );
});
export default MyStack;
