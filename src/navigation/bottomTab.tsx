import React, {memo} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from 'screen/Home';
import Options from 'screen/Options';
import LikeSongs from 'screen/LikeSongs';
import NowPlaying from 'screen/NowPlaying';
import Discover from 'screen/Discover';
import {StyleSheet, Image} from 'react-native';
const Tab = createBottomTabNavigator();
const BottomTab = memo(() => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        style: styles.tabBarStyle,
        activeTintColor: 'red',
        inactiveBackgroundColor: 'transparent',
        inactiveTintColor: '#FFF',
      }}>
      <Tab.Screen
        name={'Home'}
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => (
            <Image
              style={{tintColor: color}}
              source={require('image/Vectorhome.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Playlist'}
        component={LikeSongs}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              style={{tintColor: color}}
              source={require('image/heart.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Options'}
        component={Options}
        options={{
          tabBarIcon: ({color}) => (
            <Image
              style={{tintColor: color}}
              source={require('image/cog.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'NowPlaying'}
        component={NowPlaying}
        options={{
          tabBarLabel: 'Playing Now',
          tabBarIcon: ({color}) => (
            <Image
              style={{tintColor: color}}
              source={require('image/Music.png')}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Discover'}
        component={Discover}
        options={{
          tabBarLabel: 'Discover',
          tabBarIcon: ({color}) => (
            <Image
              style={{tintColor: color}}
              source={require('image/Discover.png')}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
});
export default BottomTab;
const styles = StyleSheet.create({
  icon: {color: '#FFF'},
  tabBarStyle: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#98CCFD',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
