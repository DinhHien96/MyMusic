import React, {memo, useState} from 'react';
import {View} from 'react-native';

interface UserType {
  name: string;
  age: number | null;
}

const RootStack = memo(() => {
  const [user, setUser] = useState<UserType>({
    name: '',
    age: null,
  });
  return <View></View>;
});
