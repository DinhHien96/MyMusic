export interface UserType {
  id: number;
  name: string;
  phoneNumber: string;
}
