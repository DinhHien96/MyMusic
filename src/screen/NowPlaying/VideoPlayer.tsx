import React, {
  Dispatch,
  memo,
  SetStateAction,
  useState,
} from 'react';
import {View, StyleSheet} from 'react-native';
import Video from 'react-native-video';

interface Progress {
  currentTime: number;
  playableDuration: number;
}
interface Props {
  pauseAndPlay: boolean;
  currentTime: number;
  setCurrentTime: Dispatch<SetStateAction<number>>;
  videoRef: any;
  playableDurationRef: any;
  isSeeking: boolean;
  audioOnly?: boolean;
  isMuted: boolean;
  isRepeat:boolean;
}

const VideoPlayer = memo(
  ({
    pauseAndPlay,
    setCurrentTime,
    playableDurationRef,
    videoRef,
    isSeeking,
    isMuted,
    isRepeat
  }: Props) => {
    const [loading, setLoading] = useState(true);

    return (
      <View style={styles.container}>
        <Video
        repeat={isRepeat}
          muted={isMuted}
          audioOnly={true || false}
          ref={videoRef}
          paused={pauseAndPlay}
          fullscreen
          controls={false}
          onLoadStart={() => {
            setLoading(true);
          }}
          onLoad={() => {
            setLoading(false);
          }}
          onSeek={({currentTime}) => {
            if (isSeeking) {
              return;
            }

            setCurrentTime(currentTime);
          }}
          onProgress={({currentTime, playableDuration}) => {
            if (isSeeking) {
              return;
            }
            setCurrentTime(currentTime);
            playableDurationRef.current = playableDuration;
          }}
          source={require('../../video/LetMeHearYourVoice.mp4')}
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
        />
      </View>
    );
  },
);
export default VideoPlayer;
const styles = StyleSheet.create({
  container: {flex: 1},
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
