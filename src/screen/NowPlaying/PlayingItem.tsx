import React, {memo} from 'react';
import {View, Image, ImageSourcePropType, Text, StyleSheet} from 'react-native';
import scaleAccordingToDevice from 'ultis/accordingDevice';

interface RecommendItemProps {
  image: ImageSourcePropType;
  name: string;
  singer: string;
}

const RecommendItem = memo(({image, name, singer}: RecommendItemProps) => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={image}
      />
      <Text style={styles.textName}>{name}</Text>
      <Text style={styles.textSinger}>{singer}</Text>
    </View>
  );
});
export default RecommendItem;
const styles = StyleSheet.create({
    container: {paddingLeft:16,alignItems:'center'},
  textName: {
    fontSize: 16,
    lineHeight: 19,
    color: '#091127',
    textAlign: 'center',
    paddingTop: 15,
  },
  textSinger: {
    fontSize: 10,
    lineHeight: 12,
    color: '#8996B8',
    textAlign: 'center',
    paddingTop: 6,
  },
  image:{
    borderRadius: 10,
    width: scaleAccordingToDevice(190),
    height: scaleAccordingToDevice(190),
  }
});
