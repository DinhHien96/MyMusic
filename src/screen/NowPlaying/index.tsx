import Slider from '@react-native-community/slider';
import React, {memo, useRef, useState} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import timeConvert from 'ultis/timeConvert';
import VideoPlayer from './VideoPlayer';
const widthDevice = Dimensions.get('window').width;

const NowPlaying = memo(({}) => {
  const videoRef = useRef<any>();

  const [currentIndex, setCurrentIndex] = useState(0);
  const [pauseAndPlay, setPauseAndPlay] = useState<boolean>(false);
  const [currentTime, setCurrentTime] = useState(0);
  const playableDuration = useRef(1);
  const [isSeeking, setIsSeeking] = useState<boolean>(false);
  const [isMute, setIsMute] = useState<boolean>(false);
  const [isRepeated, setIsRepeated] = useState<boolean>(false);
  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Playing Now</Text>
      {currentIndex === 0 ? (
        <View style={styles.audio}>
          <VideoPlayer
            isRepeat={isRepeated}
            isMuted={isMute}
            audioOnly={true}
            pauseAndPlay={pauseAndPlay}
            currentTime={currentTime}
            playableDurationRef={playableDuration}
            videoRef={videoRef}
            isSeeking={isSeeking}
            setCurrentTime={setCurrentTime}
          />
          <View style={{height: widthDevice}}>
            <Image style={styles.image} source={require('image/image1.png')} />
            <Text style={styles.nameSong}>Moment Apart</Text>
            <Text style={styles.nameSinger}>ODESZA</Text>
          </View>
          <View style={styles.controls}>
            <TouchableOpacity onPress={() => setIsMute(!isMute)}>
              {isMute === false ? (
                <Image
                  style={styles.imageVolume}
                  source={require('image/volume.png')}
                />
              ) : null}
              {isMute === true ? (
                <Image
                  source={require('image/NoAudio.png')}
                  style={styles.imageVolume}
                />
              ) : null}
            </TouchableOpacity>
            <View style={styles.optionsPlayer}>
              <TouchableOpacity onPress={() => setIsRepeated(!isRepeated)}>
                <Image
                  style={{
                    tintColor: isRepeated ? 'red' : '#8996B8',
                    marginTop: 18,
                  }}
                  source={require('image/repeat.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  style={styles.imageVolume}
                  source={require('image/outline.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : null}
      {currentIndex === 1 ? (
        <View style={styles.optionVideo}>
          <VideoPlayer
            isRepeat={isRepeated}
            isMuted={isMute}
            pauseAndPlay={pauseAndPlay}
            currentTime={currentTime}
            playableDurationRef={playableDuration}
            videoRef={videoRef}
            isSeeking={isSeeking}
            setCurrentTime={setCurrentTime}></VideoPlayer>
          <View style={styles.controls}>
            <TouchableOpacity onPress={() => setIsMute(!isMute)}>
              {isMute === false ? (
                <Image
                  style={styles.imageVolume}
                  source={require('image/volume.png')}
                />
              ) : null}
              {isMute === true ? (
                <Image
                  style={styles.imageVolume}
                  source={require('image/NoAudio.png')}
                />
              ) : null}
            </TouchableOpacity>
            <View style={styles.optionsPlayer}>
              <TouchableOpacity onPress={() => setIsRepeated(!isRepeated)}>
                <Image
                  style={{
                    tintColor: isRepeated ? 'red' : '#8996B8',
                    marginTop: 18,
                  }}
                  source={require('image/repeat.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Image
                  style={styles.imageVolume}
                  source={require('image/outline.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : null}

      <View style={styles.timePlayer}>
        <Text style={styles.time}>{timeConvert(currentTime)}</Text>
        <Text style={styles.time}>{timeConvert(playableDuration.current)}</Text>
      </View>
      <TouchableOpacity onPress={() => setCurrentIndex(1)}>
        <Text style={styles.time}>Video</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setCurrentIndex(0)}>
        <Text style={styles.time}>Audio</Text>
      </TouchableOpacity>

      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Slider
          minimumValue={0}
          maximumValue={100}
          minimumTrackTintColor="red"
          maximumTrackTintColor="#FFF"
          onSlidingStart={() => {
            setPauseAndPlay(true);
            setIsSeeking(true);
          }}
          onSlidingComplete={() => {
            setPauseAndPlay(false);
            setIsSeeking(false);
          }}
          value={(currentTime / playableDuration.current) * 100}
          onValueChange={(value: number) => {
            videoRef &&
              videoRef.current.seek((value / 100) * playableDuration.current);
          }}
          style={{width: 320, height: 50}}
        />
        <View style={styles.actions}>
          <TouchableOpacity onPress={() => {}}>
            <Image
              style={styles.buttonBack}
              source={require('image/back1.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setPauseAndPlay(!pauseAndPlay);
            }}>
            {pauseAndPlay === false ? (
              <Image
                style={styles.actionsItem}
                source={require('image/pause.png')}
              />
            ) : (
              <Image
                style={styles.actionsItem}
                source={require('image/Play.png')}
              />
            )}
          </TouchableOpacity>
          <Image
            style={styles.actionsItem}
            source={require('image/next.png')}
          />
        </View>
      </View>
    </View>
  );
});
export default NowPlaying;
const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center'},
  optionVideo: {width: widthDevice, height: widthDevice},
  buttonBack: {marginTop: 10},
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: widthDevice,
    paddingHorizontal: 18,
  },
  actionsItem: {justifyContent: 'flex-start'},
  item: {marginTop: 34},
  nameSong: {
    fontSize: 20,
    lineHeight: 22,
    textAlign: 'center',
  },
  nameSinger: {
    fontSize: 20,
    lineHeight: 22,
    color: '#8996B8',
    textAlign: 'center',
    marginTop: 6,
    marginBottom: 18,
  },
  textHeader: {
    fontSize: 24,
    lineHeight: 26,
    textAlign: 'center',
    marginTop: 32,
  },
  options: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
  },
  titleStyle: {
    fontSize: 14,
    lineHeight: 16,
  },
  itemDiscover: {
    alignItems: 'center',
  },
  image: {alignSelf: 'center', marginTop: 18},
  imageVolume: {alignSelf: 'center', marginTop: 18, tintColor: '#8996B8'},
  optionsPlayer: {flexDirection: 'row'},

  time: {fontSize: 20, lineHeight: 21, color: '#0E172D'},
  timePlayer: {
    padding: 18,
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
  },
  audio: {width: widthDevice},
  controls: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 18,
  },
});
