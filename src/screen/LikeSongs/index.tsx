import React, {memo} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageRequireSource,
  Image,
  ScrollView,
} from 'react-native';
interface YourPlaylistItem {
  image: ImageRequireSource;
  name: string;
  singer: string;
}
const PlaylistItem = [
  {
    name: 'Monsters Go Bump',
    singer: 'ERIKA RECINOS',
    image: require('image/beliver.png'),
  },
  {
    name: 'Moment Apart',
    singer: 'ODESZA',
    image: require('image/image1.png'),
  },
  {
    name: 'Believer',
    singer: 'IMAGE DRAGONS',
    image: require('image/beliver.png'),
  },
  {
    name: 'ShortWave',
    singer: 'Ryan Phillip',
    image: require('image/image1.png'),
  },
];
const LikeSongs = memo(() => {
  return (
    <>
      <Text style={styles.textHeader}>Liked Songs</Text>
      <ScrollView style={styles.container}>
        {PlaylistItem.map((item: YourPlaylistItem, index) => {
          return (
            <View  key={index} style={styles.itemLike}>
              <Image style={styles.image} source={item.image} />
              <View>
                <Text style={styles.textName}>{item.name}</Text>
                <Text style={styles.textSinger}>{item.singer}</Text>
              </View>
            </View>
          );
        })}
      </ScrollView>
    </>
  );
});

export default LikeSongs;
const styles = StyleSheet.create({
  container: {flex: 1},
  textHeader: {
    fontSize: 24,
    lineHeight: 26,
    marginTop: 32,
    textAlign: 'center',
    color: '#091127',
  },
  textSinger: {
    fontSize: 12,
    lineHeight: 14,
    color: '#8996B8',
    marginTop: 20,
    marginLeft:10
    
  },
  textName: {
    fontSize: 18,
    lineHeight: 20,
    justifyContent: 'flex-end',
    marginLeft: 10,
    marginTop: 24,
  },
  information: {justifyContent: 'center', marginLeft: 20},
  itemLike: {flexDirection: 'row', marginTop: 20, marginLeft: 16},
  image: {borderRadius: 10},
});
