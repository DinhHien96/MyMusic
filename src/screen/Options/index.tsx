import {useNavigation} from '@react-navigation/native';
import React, {memo} from 'react';
import Routes from 'configs/Routes';
import {
  Text,
  View,
  ImageSourcePropType,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
interface OptionsArrProps {
  title: string;
  image: ImageSourcePropType;
  route?: any;
}
const OptionsArr = [
  {
    title: 'Like Songs',
    image: require('image/heart.png'),
    route: Routes.LikeSongs,
  },
  {title: 'Language', image: require('image/Vector.png')},
  {title: 'Contact Us', image: require('image/contact.png')},
  {title: 'Settings', image: require('image/cog.png'),route: Routes.Settings},
];
const Options = memo(() => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text style={styles.textHeader}>Settings</Text>
      {OptionsArr.map((item: OptionsArrProps, index) => {
        return (
          <View style={styles.container}>
          <TouchableOpacity
          key={index}
            onPress={() => navigation.navigate(item.route)}
            style={styles.container}>
            <View style={styles.itemStyle}>
              <Image style={styles.image} source={item.image} />
              <Text style={styles.textTitle}>{item.title}</Text>
            </View>
          </TouchableOpacity>
          </View>
        );
      })}
    </View>
  );
});

export default Options;
const styles = StyleSheet.create({
  container: {},
  image: {},
  textTitle: {
    fontSize: 18,
    lineHeight: 20,
    marginLeft: 14,
    alignSelf: 'center',
  },
  itemStyle: {flexDirection: 'row', marginTop: 20, marginLeft: 16},
  textHeader: {fontSize:24,lineHeight:26, alignSelf: 'center',marginTop:32}
});
