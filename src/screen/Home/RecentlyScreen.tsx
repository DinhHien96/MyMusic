import React, {memo} from 'react';
import {Text, View, StyleSheet, ImageRequireSource, Image} from 'react-native';
import scaleAccordingToDevice from 'ultis/accordingDevice';
interface YourPlaylistItem {
  image: ImageRequireSource;
  name: string;
  singer: string;
}
const RecentlyScreen = memo(({image, name, singer}: YourPlaylistItem) => {
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={image} />
      <View style={styles.information}>
        <Text style={styles.textName}>{name}</Text>
        <Text style={styles.textSinger}>{singer}</Text>
      </View>
    </View>
  );
});
export default RecentlyScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 16,
  },
  textHeader: {
    fontSize: 24,
    lineHeight: 29,
    marginLeft: 28,
  },
  textSinger: {
    fontSize: 12,
    lineHeight: 14,
    color: '#8996B8',
    textAlign: 'center',
  },
  textName: {
    fontSize: 16,
    lineHeight: 18,
    justifyContent: 'flex-end',
  },
  information: {
    marginLeft: 20,
    justifyContent: 'center',
  },
  image: {
    width: scaleAccordingToDevice(100),
    height: scaleAccordingToDevice(100),
  },
});
