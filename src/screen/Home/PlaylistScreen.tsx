import React, {memo} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageRequireSource,
  Image,
  TouchableOpacity,
} from 'react-native';
import scaleAccordingToDevice from 'ultis/accordingDevice';
interface YourPlaylistItem {
  image: ImageRequireSource;
  name: string;
  singer: string;
}
const PlaylistScreen = memo(({image, name, singer}: YourPlaylistItem) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
      <Image style={styles.image} source={image} />
      </TouchableOpacity>
      
      <View style={styles.information}>
        <Text style={styles.textName}>{name}</Text>
        <Text style={styles.textSinger}>{singer}</Text>
      </View>
    </View>
  );
});

export default PlaylistScreen;
const styles = StyleSheet.create({
  container: {flexDirection: 'row'},
  textHeader: {fontSize: 26, lineHeight: 29, marginLeft: 28},
  textSinger: {
    fontSize: 12,
    lineHeight: 14,
    color: '#8996B8',
    textAlign: 'center',
  },
  textName: {fontSize: 18, lineHeight: 18, justifyContent: 'flex-end'},
  information: {justifyContent: 'center', marginLeft: 20},
  image: {
    width: scaleAccordingToDevice(130),
    height: scaleAccordingToDevice(130),
    marginLeft:18,marginTop:18
  },
});
