import React, {memo, useState} from 'react';
import {
  Text,
  View,
  ScrollView,
  ImageSourcePropType,
  TouchableHighlight,
  StyleSheet,
  SafeAreaView,
  FlatList,
  Image,
} from 'react-native';
import RecommendItem from './item';
import PlaylistScreen from './PlaylistScreen';

interface RecommendItemProps {
  image: ImageSourcePropType;
  name?: string;
  singer?: string;
}

const PlaylistItem = [
  {
    name: 'Monsters Go Bump',
    singer: 'ERIKA RECINOS',
    image: require('image/beliver.png'),
  },
  {
    name: 'Moment Apart',
    singer: 'ODESZA',
    image: require('image/image1.png'),
  },
  {
    name: 'Believer',
    singer: 'IMAGE DRAGONS',
    image: require('image/beliver.png'),
  },
  {
    name: 'ShortWave',
    singer: 'Ryan Phillip',
    image: require('image/image1.png'),
  },
];

const arr = ['Playlist', 'Recently'];

const Home = memo(() => {
  const [currentIndex, setCurrentIndex] = useState(0);
  return (
    <View style={styles.container}>
      <ScrollView>
        <SafeAreaView>
          <Text style={styles.textHeader}>Recommended for you</Text>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.scrollContainer}
            horizontal={true}>
            {PlaylistItem.map((items: RecommendItemProps, index) => {
              return (
                <RecommendItem
                  key={index}
                  image={items.image}
                  singer={items.singer}
                  name={items.name}
                />
              );
            })}
          </ScrollView>
        </SafeAreaView>
        <View style={styles.itemsSelect}>
          {arr.map((item, index) => {
            return (
              <TouchableHighlight
                style={styles.button}
                activeOpacity={0.7}
                underlayColor="#8AE3DD"
                key={index}
                onPress={() => {
                  setCurrentIndex(index);
                }}>
                <Text style={styles.textHeader}>{item}</Text>
              </TouchableHighlight>
            );
          })}
        </View>
        <View>
          {currentIndex === 0 ? (
            <View style={styles.scrollContainer}>
              {PlaylistItem.map((item, index) => {
                return (
                  <PlaylistScreen
                    key={index}
                    name={item.name}
                    singer={item.singer}
                    image={item.image}
                  />
                );
              })}
            </View>
          ) : null}
          {currentIndex === 1 ? (
            <View style={styles.scrollContainer}>
              {PlaylistItem.map((item, index) => {
                return (
                  <PlaylistScreen
                    key={index}
                    name={item.name}
                    singer={item.singer}
                    image={item.image}
                  />
                );
              })}
            </View>
          ) : null}
        </View>
      </ScrollView>
    </View>
  );
});

export default Home;
const styles = StyleSheet.create({
  container: {flex: 1},
  textHeader: {fontSize: 24, lineHeight: 29, marginLeft: 24, marginTop: 10},
  scrollContainer: {marginTop: 20, marginLeft: 8},
  button: {alignSelf: 'center', marginLeft: 18},
  itemsSelect: {flexDirection: 'row', marginTop: 24},
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  image: {},
});
