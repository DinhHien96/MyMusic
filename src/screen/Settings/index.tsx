import React, {memo} from 'react';
import {Pressable, StyleSheet, Text, View} from 'react-native';
const Settings = memo(() => {
  return (
    <View style={styles.container}>
      <Pressable
        style={({pressed}) => [
          {
            backgroundColor: pressed ? '#8AE3DD' : '#FFF',
          },
          styles.button,
        ]}
        //   activeOpacity={0.5} underlayColor="#8AE3DD"
      >
        <Text style={styles.text}>Light Mode</Text>
      </Pressable>
      <Pressable
        style={({pressed}) => [
          {
            backgroundColor: pressed ? '#8AE3DD' : '#FFF',
          },
          styles.button,
        ]}>
        <Text style={styles.text}>Dark Mode</Text>
      </Pressable>
    </View>
  );
});
export default Settings;
const styles = StyleSheet.create({
  container: {flex: 1},
  text: {fontSize: 24, lineHeight: 26, marginTop: 10, marginLeft: 24},
  button: {
    borderRadius: 8,
    padding: 6,
  },
});
