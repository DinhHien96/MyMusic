import React, {memo} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import ItemDiscover from './itemDiscover';
import {useNavigation} from '@react-navigation/native';
import Routes from 'configs/Routes';

const OptionsDiscover = [
  {image: require('image/NewSongs1.png'), title: 'New Songs'},
  {image: require('image/options2.png'), title: 'Category'},
  {image: require('image/star.png'), title: 'Top 100'},
  {image: require('image/TopMV1.png'), title: 'Top MV'},
];
const PlaylistItem = [
  {
    name: 'Monsters Go Bump',
    singer: 'ERIKA RECINOS',
    image: require('image/beliver.png'),
  },
  {
    name: 'Moment Apart',
    singer: 'ODESZA',
    image: require('image/image1.png'),
  },
  {
    name: 'Believer',
    singer: 'IMAGE DRAGONS',
    image: require('image/beliver.png'),
  },
  {
    name: 'ShortWave',
    singer: 'Ryan Phillip',
    image: require('image/image1.png'),
  },
];
const Discover = memo(route => {
  const navigation = useNavigation();
  const onPress = () => navigation.navigate(Routes.LikeSongs);
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.textHeader}>Discover</Text>
      <View style={styles.options}>
        {OptionsDiscover.map((item, index) => {
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate(Routes.VideoPlayer)}
              key={index.toString()}
              style={styles.itemDiscover}>
              <Image style={styles.image} source={item.image} />
              <Text style={styles.titleStyle}>{item.title}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.textList}>RECENTLY</Text>
      </TouchableOpacity>

      <View style={styles.options}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={styles.scrollContainer}>
          {PlaylistItem.map((item, index) => {
            return (
              <TouchableOpacity key={index.toString()}>
                <ItemDiscover
                  image={item.image}
                  name={item.name}
                  singer={item.singer}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.textList}>NEW EDM</Text>
      </TouchableOpacity>

      <View style={styles.options}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={styles.scrollContainer}>
          {PlaylistItem.map((item, index) => {
            return (
              <TouchableOpacity key={index.toString()}>
                <ItemDiscover
                  image={item.image}
                  name={item.name}
                  singer={item.singer}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    </ScrollView>
  );
});
export default Discover;
const styles = StyleSheet.create({
  container: {flex: 1},
  nameSong: {
    fontSize: 24,
    lineHeight: 26,
    color: '#091127',
    textAlign: 'center',
    paddingTop: 15,
  },
  nameSinger: {
    fontSize: 20,
    lineHeight: 22,
    color: '#8996B8',
    textAlign: 'center',
    paddingTop: 6,
  },
  textHeader: {
    fontSize: 32,
    lineHeight: 34,
    textAlign: 'center',
    marginTop: 32,
  },
  options: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleStyle: {
    fontSize: 14,
    lineHeight: 16,
  },
  itemDiscover: {marginLeft: 18, marginTop: 18},
  image: {marginRight: 16},
  scrollContainer: {
    marginTop: 24,
  },
  textList: {
    marginLeft: 18,
    lineHeight: 24,
    fontSize: 22,
    marginTop: 32,
  },
});
