import React, {memo} from 'react';
import {StyleSheet,Text,ImageRequireSource,ScrollView, Image} from 'react-native'
interface DiscoverItemProps {
    image: ImageRequireSource;
    name: string;
    singer: string;
  }
const ItemDiscover = memo(({image,name, singer}:DiscoverItemProps)=>{
    return(
        <ScrollView 
        
        contentContainerStyle={styles.scrollContainer}>
            
            <Image source={image}/>
            <Text style={styles.textName}>{name}</Text>
            <Text style={styles.textSinger}>{singer}</Text>

        </ScrollView>
    )
})
export default  ItemDiscover;
const styles = StyleSheet.create({
    scrollContainer:{marginLeft:18},
    textName: {
        fontSize: 16,
        lineHeight: 19,
        color: '#091127',
        textAlign: 'center',
        paddingTop: 15,
      },
      textSinger: {
        fontSize: 10,
        lineHeight: 12,
        color: '#8996B8',
        textAlign: 'center',
        paddingTop: 6,
      },
})