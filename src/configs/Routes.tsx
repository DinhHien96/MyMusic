const Routes = {
  Home: 'Home',
  Playlist: 'Playlist',
  Options: 'Options',
  BottomTab: 'BottomTab',
  LikeSongs: 'LikeSongs',
  Settings: 'Settings',
  NowPlaying: 'NowPlaying',
  Discover: 'Discover',
  VideoPlayer: 'VideoPlayer',
  SeekBar: 'SeekBar',
};
export default Routes;
