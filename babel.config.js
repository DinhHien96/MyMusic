module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@components': './src/components',
          '@components/*': ['src/components/*'],
          '@screens/*': ['src/screens/*'],
          '@configs/*': ['src/configs/*'],
          '@navigations/*': ['src/navigations/*'],
          '@images/*': ['src/images/*'],
          '@configs': ['src/configs/index'],
        },
      },
    ],
  ],
};
